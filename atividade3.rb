require "json"

file = File.read('./pokemons.json')
minha_json = JSON.parse(file)
# Array que pega a array results 
hash_arr = minha_json["results"]
resultado = []                        # criei um array resultado vazio
hash_arr.each do |hash|
  b = hash["name"].split("-",-1)
  resultado << b[0]
end

distintos = resultado.uniq
resultado2 = []

distintos.each do |hash|
  c = hash.slice(0)
  resultado2 << c[0]
end

top3 = resultado2.sort

hash1 = {}

top3.uniq.each do |element|
  hash1[element] = 1
end 

top3.each do |element|
  hash1[element] += 1
end 

orden = hash1.sort_by{|_key,value|value}
pokemon = [*orden].reverse

3.times do |element|
  puts pokemon[element].slice(0)  
end
