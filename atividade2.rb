require "json"

file = File.read('./pokemons.json')
minha_json = JSON.parse(file)
# Array que pega a array results 
hash_arr = minha_json["results"]
resultado = []                        # criei um array resultado vazio
hash_arr.each do |hash|               # loop pra pegar o valores da hash
    b = hash["name"].split("-",-1)    # b é uma string, hash["name"] pega os nomes da array results, split para pegar apenas o primeiro nome
    resultado << b[0]                 # acrescentei os nomes na array resultado
end
top10 = resultado.sort         

hash1 = {}

# cria um hash vazio e começa a iterar sobre a matriz. Para cada elemento, ele verifica primeiro se o elemento já existe como chave na matriz. Se isso acontecer, ele adicionará 1 a ele, aumentando a contagem. Se ele não foi encontrado anteriormente na matriz, ele criará a nova chave e fornecerá um valor inicial de 1 

top10.uniq.each do |element|
    hash1[element] = 1
end 

top10.each do |element|
    hash1[element] += 1
end 

orden = hash1.sort_by{|_key,value|value}
pokemon = [*orden].reverse

10.times do |element|
    puts pokemon[element].slice(0)  
end

